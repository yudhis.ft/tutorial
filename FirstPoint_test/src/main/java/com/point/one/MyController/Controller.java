package com.point.one.MyController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@org.springframework.stereotype.Controller
public class Controller {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String hello(){
        return "hello";
    }
    @RequestMapping(value = "/redirect_page", method = RequestMethod.GET)
    public String redirect(){
        return "redirect:index";
    }
    @RequestMapping(value = "/index", method = RequestMethod.GET)
    public String index(){
        return "index";
    }
}
