<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix = "sql"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page import = "java.io.*,java.util.*,java.sql.*"%>
<%@ page import = "javax.servlet.http.*,javax.servlet.*" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body>
        <sql:setDataSource
                var = "snapshot"
                driver = "com.mysql.jdbc.Driver"
                url = "jdbc:mysql://localhost:3306/hackathon"
                user = "root"
                password = ""/>
        <sql:query var= "result" dataSource="${snapshot}">
            SELECT * from AR_LOCATION;
        </sql:query>
        <h1>Index Page</h1>
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">Location_ID</th>
                    <th scope="col">Location_Name</th>
                    <th scope="col">Location_Address</th>
                    <th scope="col">Location_Picture</th>
                    <th scope="col">Longitude</th>
                    <th scope="col">Latitude</th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="row" items = "${result.rows}">
                    <tr>
                        <td><c:out value="${row.location_id}"/></td>
                        <td><c:out value="${row.location_name}"/></td>
                        <td><c:out value="${row.location_address}"/></td>
                        <td><c:out value="${row.location_picture}"/></td>
                        <td><c:out value="${row.longitude}"/></td>
                        <td><c:out value="${row.latitude}"/></td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </body>
</html>