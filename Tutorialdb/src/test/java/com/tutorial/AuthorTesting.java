package com.tutorial;

import com.tutorial.dao.authorRepository;
import com.tutorial.domain.Author;
import com.tutorial.domain.AuthorDetails;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@EntityScan("com.tutorial.domain")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@DataJpaTest
public class AuthorTesting {
    @TestConfiguration
    static class configuration{
        @Bean
        public authorRepository dao(){
            return new authorRepository();
        }

        @Autowired
        authorRepository dao;

        private Author author;
        private AuthorDetails authorDetails;
        @Before
        public void setup(){
            author = new Author();
            author.setAuthorId(1);
            author.setFirstName("Yudhistira");
            author.setLastName("Thamolua");
            author.setAuthorDetails(authorDetails);
        }

        @Test
        public void save(){dao.save(author);}
    }
}
