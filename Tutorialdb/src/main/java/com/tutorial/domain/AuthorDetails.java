package com.tutorial.domain;

import javax.persistence.*;

@Entity
@Table(name = "AuthorDetails")
public class AuthorDetails {
    @Id
    @GeneratedValue(generator = "authorIdGenerator", strategy = GenerationType.AUTO)
    @Column(name = "authorId") private int authorId;

    @Column(name = "pseudonym") private String pseudonym;

    @OneToOne(mappedBy = "authorDetails")
    private Author author;

    public int getAuthorId() {
        return authorId;
    }

    public void setAuthorId(int authorId) {
        this.authorId = authorId;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public Author getAuthor() {
        return author;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
