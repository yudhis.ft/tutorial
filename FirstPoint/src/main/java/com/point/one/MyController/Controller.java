package com.point.one.MyController;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@org.springframework.stereotype.Controller
public class Controller {
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String hello(){
        return "hello";
    }
}
